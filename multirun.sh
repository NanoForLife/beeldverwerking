#!/bin/bash

for batch_size in {5..30..5}
do
    for lr in 0.1 0.01 0.001 0.0001
    do
        python train.py --wandb_project=beeldverwerking --batch_size=$batch_size --lr=$lr
    done
done

